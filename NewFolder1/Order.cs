namespace CoffeeManagement.NewFolder1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Order")]
    public partial class Order
    {
        public int OrderID { get; set; }

        [Required]
        [StringLength(50)]
        public string StaffID { get; set; }

        [StringLength(200)]
        public string Note { get; set; }

        public int PayMethodID { get; set; }

        public DateTime Date { get; set; }

        public decimal TotalAmount { get; set; }

        public virtual Account Account { get; set; }

        public virtual PaymentMethod PaymentMethod { get; set; }

        public virtual Table Table { get; set; }
    }
}
