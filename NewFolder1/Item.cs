namespace CoffeeManagement.NewFolder1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Item")]
    public partial class Item
    {
        public int ItemID { get; set; }

        [Required]
        [StringLength(50)]
        public string ItemName { get; set; }

        public int CategoryID { get; set; }

        public decimal Price { get; set; }

        public virtual Category Category { get; set; }
    }
}
