﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeManagement
{
    public partial class Category1 : Form
    {
        public Category1()
        {
            InitializeComponent();
        }

        private void Category_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            HomePage p = new HomePage();
            this.Hide();    
            p.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Bill b = new Bill();
            this.Hide();
            b.ShowDialog();
        }

        private void btnCategoryAdd_Click(object sender, EventArgs e)
        {
            
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Account a = new Account();
            this.Hide();
            a.ShowDialog();
        }

        private void btnBillMenu_Click(object sender, EventArgs e)
        {
            Menu m = new Menu();
            this.Hide();
            m.ShowDialog();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            HomePage h = new HomePage();
            this.Hide();
            h.ShowDialog();
        }
    }
}
