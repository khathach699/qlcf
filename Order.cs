﻿using CoffeeManagement.NewFolder1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeManagement
{
    public partial class Order : Form
    {

        public Order()
        {
            InitializeComponent();
        }

        private void Order_Load(object sender, EventArgs e)
        {
            Model1 context = new Model1();

            List<Category> cate = context.Categories.ToList();
            comboBox1.DataSource = cate;
            comboBox1.DisplayMember = "CategoryName";
            comboBox1.ValueMember = "CategoryID";

            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Model1 context = new Model1();
            List<Item> listItem = context.Items.ToList();
            if (comboBox1.SelectedIndex == 1)
                ShowItem(listItem, 1);
            else if (comboBox1.SelectedIndex == 0)
                ShowItem(listItem, 2);
        }


        private void ShowItem(List<Item> item, int n)
        {

            dataGridView1.Rows.Clear();
            foreach (Item iTEM in item)
            {
                if (iTEM.CategoryID == n)
                {
                    int newRow = dataGridView1.Rows.Add(iTEM);
                    dataGridView1.Rows[newRow].Cells[0].Value = iTEM.ItemName;
                    dataGridView1.Rows[newRow].Cells[1].Value = iTEM.Price;
                    dataGridView1.Rows[newRow].Cells[2].Value = iTEM.ItemID;
                }

            }
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        string item;
        decimal price, total;
        int num = 0, id;
        decimal sum = 0;
       
        //private void ShowOrder(List<Order1> order)
        //{
        //    int newRow = dgvOrder.Rows.Add();
        //    dgvOrder.Rows[newRow].Cells[0].Value = num;
        //    dgvOrder.Rows[newRow].Cells[1].Value = item;
        //    dgvOrder.Rows[newRow].Cells[2].Value = numericUpDown1.Value;
        //    dgvOrder.Rows[newRow].Cells[3].Value = price;
        //    dgvOrder.Rows[newRow].Cells[4].Value = total;
        //    dgvOrder.Rows[newRow].Cells[5].Value = id;

        //}




        private void dgvOrder_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Lấy thông tin của dòng được click
            int row = e.RowIndex;
            int quantity = int.Parse(dgvOrder.Rows[row].Cells[1].Value.ToString());

            // Nếu số lượng lớn hơn 0, trừ đi 1
            if (quantity > 0)
            {
                if (numericUpDown1.Value <= quantity || quantity == 1)
                    dgvOrder.Rows[row].Cells[1].Value = quantity - numericUpDown1.Value;
                else
                    MessageBox.Show("The quantity you want to delete is greater than the existing quantity!!!");
                numericUpDown1.Value = 1;

                // Cập nhật tổng tiền của item
                dgvOrder.Rows[row].Cells[3].Value = decimal.Parse(dgvOrder.Rows[row].Cells[1].Value.ToString()) * decimal.Parse(dgvOrder.Rows[row].Cells[2].Value.ToString());

                // Cập nhật tổng tiền của toàn bộ đơn hàng
                sum = 0;
                foreach (DataGridViewRow a in dgvOrder.Rows)
                {
                    sum += decimal.Parse(a.Cells[3].Value.ToString());
                }
                txtTotal.Text = sum.ToString();
            }
            else
            {
                dgvOrder.Rows.RemoveAt(row);

                // Cập nhật tổng tiền của toàn bộ đơn hàng
                sum = 0;
                foreach (DataGridViewRow a in dgvOrder.Rows)
                {
                    sum += decimal.Parse(a.Cells[3].Value.ToString());
                }
                txtTotal.Text = sum.ToString();
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            string name = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            decimal p = (decimal) dataGridView1.Rows[e.RowIndex].Cells[1].Value;
            int value = (int)dataGridView1.Rows[e.RowIndex].Cells[2].Value;
            item = name;
            id = value;
            price = p;
 
            // Kiểm tra xem item đã tồn tại trong dgvOrder chưa
            bool itemExists = false;
            foreach (DataGridViewRow row in dgvOrder.Rows)
            {
                if (row.Cells[4].Value.ToString() == id.ToString())
                {
                    itemExists = true;
                    break;
                }
            }
            if (itemExists)
            {
                // Tìm kiếm dòng trong dgvOrder tương ứng với item
                int index = dgvOrder.Rows.IndexOf(dgvOrder.Rows.Cast<DataGridViewRow>().Where(row => row.Cells[4].Value.ToString() == id.ToString()).FirstOrDefault());

                // Tăng số lượng của item lên
                dgvOrder.Rows[index].Cells[1].Value = int.Parse(dgvOrder.Rows[index].Cells[1].Value.ToString()) + numericUpDown1.Value;
                numericUpDown1.Value = 1;

                // Cập nhật tổng tiền của item
                dgvOrder.Rows[index].Cells[3].Value = decimal.Parse(dgvOrder.Rows[index].Cells[1].Value.ToString()) * decimal.Parse(dgvOrder.Rows[index].Cells[2].Value.ToString());
            }
            // Nếu item chưa tồn tại, thêm item mới vào dgvOrder
            else
            {
                // Thêm item mới vào dgvOrder
                int newRow = dgvOrder.Rows.Add();
                dgvOrder.Rows[newRow].Cells[0].Value = name;
                dgvOrder.Rows[newRow].Cells[1].Value = 1;
                dgvOrder.Rows[newRow].Cells[2].Value = price;
                dgvOrder.Rows[newRow].Cells[3].Value = price;
                dgvOrder.Rows[newRow].Cells[4].Value = id;
            }
            // Cập nhật tổng tiền của toàn bộ đơn hàng
            sum = 0;
            foreach (DataGridViewRow row in dgvOrder.Rows)
            {
                sum += decimal.Parse(row.Cells[3].Value.ToString());
            }
            txtTotal.Text = sum.ToString();

        }
        int i = 0;
        //private void btnPayment_Click(object sender, EventArgs e)
        //{
        //    // Tạo một danh sách các đối tượng Order1
        //    List<OrderDetail1> orders = new List<OrderDetail1>();

        //    // Duyệt qua danh sách các dòng trong dgvOrder và thêm các đối tượng Order1 tương ứng vào danh sách orders
            
        //    foreach (DataGridViewRow row in dgvOrder.Rows)
        //    {
        //        OrderDetail order1 = new OrderDetail();
        //        //order1.OrderID = i+1;
        //        order1.ItemID = int.Parse(row.Cells[4].Value.ToString());
        //        //order1.Item.ItemName = row.Cells[0].Value.ToString();
        //        order1.Quantity = int.Parse(row.Cells[1].Value.ToString());
        //        //order1.Item.Price = decimal.Parse(row.Cells[2].ToString());
        //        order1.Total = decimal.Parse(row.Cells[3].Value.ToString());

        //        orders.Add(order1);
        //    }

        //    // Lưu các đối tượng Order1 vào CSDL
        //    Model1 context = new Model1();
        //    context.OrderDetails.AddRange(orders);
        //    context.SaveChanges();

        //    // Thông báo cho người dùng rằng dữ liệu đã được lưu thành công
        //    MessageBox.Show("Lưu dữ liệu thành công!");
        //}


        }
}
