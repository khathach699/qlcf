using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace CoffeeManagement
{
    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<CATEGORY> CATEGORies { get; set; }
        public virtual DbSet<ITEM> ITEMs { get; set; }
        public virtual DbSet<OFFICE> OFFICEs { get; set; }
        public virtual DbSet<ORDER> ORDERs { get; set; }
        public virtual DbSet<Order1> Order1 { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<TABLE> TABLEs { get; set; }
        public virtual DbSet<USER_ACCOUNT> USER_ACCOUNT { get; set; }
        public virtual DbSet<method> methods { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CATEGORY>()
                .HasMany(e => e.ITEMs)
                .WithRequired(e => e.CATEGORY)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ITEM>()
                .Property(e => e.Price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ITEM>()
                .HasMany(e => e.ORDERs)
                .WithRequired(e => e.ITEM)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ITEM>()
                .HasMany(e => e.Order1)
                .WithRequired(e => e.ITEM)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OFFICE>()
                .HasMany(e => e.USER_ACCOUNT)
                .WithRequired(e => e.OFFICE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ORDER>()
                .Property(e => e.Total)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order1>()
                .Property(e => e.Total)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order1>()
                .Property(e => e.PaymentMethod)
                .IsFixedLength();

            modelBuilder.Entity<Order1>()
                .Property(e => e.Method)
                .IsFixedLength();

            modelBuilder.Entity<USER_ACCOUNT>()
                .Property(e => e.UserName)
                .IsFixedLength();

            modelBuilder.Entity<USER_ACCOUNT>()
                .Property(e => e.Password)
                .IsFixedLength();

            modelBuilder.Entity<USER_ACCOUNT>()
                .Property(e => e.Email)
                .IsFixedLength();

            modelBuilder.Entity<USER_ACCOUNT>()
                .HasMany(e => e.ORDERs)
                .WithRequired(e => e.USER_ACCOUNT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<USER_ACCOUNT>()
                .HasMany(e => e.Order1)
                .WithRequired(e => e.USER_ACCOUNT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<method>()
                .Property(e => e.ID)
                .IsFixedLength();
        }
    }
}
