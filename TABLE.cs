namespace CoffeeManagement
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TABLE")]
    public partial class TABLE
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TableID { get; set; }

        public int? OrderID { get; set; }

        public virtual ORDER ORDER { get; set; }

        public virtual Order1 Order1 { get; set; }
    }
}
