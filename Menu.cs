﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeManagement
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            Account a = new Account();
            this.Close(); 
            a.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Bill b = new Bill();
            this.Hide();
            b.ShowDialog();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Category1 c = new Category1();
            this.Close();
            c.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            HomePage h = new HomePage();
            this.Hide();
            h.ShowDialog();

        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            HomePage h = new HomePage();
            this.Hide();
            h.ShowDialog();
        }
    }
}
