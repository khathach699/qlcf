namespace CoffeeManagement
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Order1
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order1()
        {
            TABLEs = new HashSet<TABLE>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrderID { get; set; }

        public int ItemID { get; set; }

        public int AccID { get; set; }

        public int Quantity { get; set; }

        public decimal Total { get; set; }

        [Required]
        [StringLength(30)]
        public string PaymentMethod { get; set; }

        public DateTime Date { get; set; }

        [Required]
        [StringLength(10)]
        public string Method { get; set; }

        public virtual ITEM ITEM { get; set; }

        public virtual USER_ACCOUNT USER_ACCOUNT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TABLE> TABLEs { get; set; }
    }
}
