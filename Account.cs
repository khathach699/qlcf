﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeManagement
{
    public partial class Account : Form
    {
        public Account()
        {
            InitializeComponent();
        }

        private void btnHomePage_Click(object sender, EventArgs e)
        {
            HomePage h = new HomePage();
            this.Hide();    
            h.ShowDialog(); 
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Category1 c = new Category1();
            this.Hide();
            c.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Bill b = new Bill();
            this.Hide();
            b.ShowDialog();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            Menu m = new Menu();
            this.Hide();
            m.ShowDialog();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Menu m = new Menu();
            this.Hide();
            m.ShowDialog();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            HomePage h = new HomePage();
            this.Hide();
            h.ShowDialog();
        }
    }
}
