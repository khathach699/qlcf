﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeManagement
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
             HomePage oder = new HomePage();
            this.Hide();
            oder.ShowDialog();
        }

        private void LoginFormExit_Click(object sender, EventArgs e)
        {
            
           DialogResult result =  MessageBox.Show("Do you want to exit ", "Notification", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.Yes) { 
                this.Close();
            }
        }
    }
}
