﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoffeeManagement
{
    public partial class Bill : Form
    {
        public Bill()
        {
            InitializeComponent();
        }

        private void button10_Click(object sender, EventArgs e)
        {
           HomePage o = new HomePage ();
            this.Hide();
            o.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button14_Click(object sender, EventArgs e)
        {
            Category1 c = new Category1();
            this.Hide ();
            c.ShowDialog();
        }

        private void Bill_Load(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            Account a = new Account();
            this.Hide();
            a.ShowDialog();
        }

        private void btnBillMenu_Click(object sender, EventArgs e)
        {
            Menu m = new Menu();
            this.Hide();
            m.ShowDialog();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
             HomePage h = new HomePage();
             this.Hide();
             h.ShowDialog();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
